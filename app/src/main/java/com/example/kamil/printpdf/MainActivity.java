package com.example.kamil.printpdf;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText);
    }

    public void makepdf(View v) {
        if (text.getText().length() != 0) {
            generatePDF(text.getText().toString());
            Toast.makeText(MainActivity.this, "pdf created", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "text is empty", Toast.LENGTH_SHORT).show();
        }
    }

    String getFileName() {
        Calendar mCalendar;
        mCalendar = Calendar.getInstance();
        int mMil = mCalendar.get(Calendar.MILLISECOND);
        int mSec = mCalendar.get(Calendar.SECOND);
        int mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = mCalendar.get(Calendar.MINUTE);
        int mYear = mCalendar.get(Calendar.YEAR);
        int mMonth = mCalendar.get(Calendar.MONTH) + 1;
        int mDay = mCalendar.get(Calendar.DATE);
        return "pdf" + mYear + mMonth + mDay + mHour + mMinute + mSec + mMil;
    }

    void generatePDF(String text) {
        String FILE = Environment.getExternalStorageDirectory().toString()
                + "/testPDF/" + getFileName()+".pdf";
        String FOLDER = Environment.getExternalStorageDirectory().toString()
                + "/testPDF/";
        File folder = new File(FOLDER);
        folder.mkdir();
        Document document = new Document(PageSize.A4);
        File myDir = new File(FILE);
        try {
            myDir.createNewFile();
        } catch (IOException e) {
        }
        try {
            try {
                PdfWriter.getInstance(document, new FileOutputStream(FILE));
            } catch (DocumentException e) {
                Log.e(TAG, e.getMessage());
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        document.open();

        try {
            addContent(document, text);
        } catch (DocumentException e) {
            Log.e(TAG, e.getMessage());
        }
        document.close();
    }

    void addContent(Document document, String string) throws DocumentException {
        Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        Paragraph prProfile = new Paragraph();
        prProfile.setFont(smallBold);
        prProfile.add("\n" + string + "\n ");
        prProfile.setFont(smallBold);
        document.add(prProfile);

        // Create new Page in PDF
        document.newPage();
    }
}
